"use strict";

const urlStreamer = require("./url-streamer");
const setHours = require("date-fns/set_hours");
const setMinutes = require("date-fns/set_minutes");
const setSeconds = require("date-fns/set_seconds");
const setMilliseconds = require("date-fns/set_milliseconds");
const isBefore = require("date-fns/is_before");
var differenceInDays = require("date-fns/difference_in_days");
var addDays = require("date-fns/add_days");
const parse = require("date-fns/parse");

// https://about.gitlab.com/handbook/engineering/quality/issue-triage/#priority
const PRIORITY_SLA_DAYS = {
  "priority::1": 30,
  "priority::2": 90,
};

// https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity
const SEVERITY_SLA_DAYS = {
  "severity::1": 30,
  "severity::2": 60,
  "severity::3": 90,
};

function parseLocalDate(date) {
  let d = parse(date);
  d = setHours(d, 0);
  d = setMinutes(d, 0);
  d = setSeconds(d, 0);
  d = setMilliseconds(d, 0);
  return d;
}

function isPastDue(date) {
  let d = parseLocalDate(date);
  return isBefore(d, Date.now());
}

class InfradevIssueCategorizer {
  constructor() {}

  async getLabelEvents(issue) {
    let firstEvents = new Map();
    for await (const event of urlStreamer(`https://gitlab.com/api/v4/projects/${issue.project_id}/issues/${issue.iid}/resource_label_events`)) {
      if (event.action !== "add" || !event.label) {
        continue;
      }

      let createdAt = parse(event.created_at);
      let label = event.label.name;
      if (!firstEvents.has(label)) {
        firstEvents.set(label, createdAt);
      }
    }

    return firstEvents;
  }

  async categorizeRelatedIssues(issue) {
    let incidentRelations = [];
    let capacityPlanningRelations = [];

    for await (const link of urlStreamer(`https://gitlab.com/api/v4/projects/${issue.project_id}/issues/${issue.iid}/links`)) {
      if (!link.web_url) continue;

      if (link.web_url.startsWith("https://gitlab.com/gitlab-com/gl-infra/production/")) {
        incidentRelations.push(link.web_url);
      } else if (link.web_url.startsWith("https://gitlab.com/gitlab-com/gl-infra/capacity-planning/")) {
        capacityPlanningRelations.push(link.web_url);
      }
    }

    return {
      incidents: incidentRelations,
      capacityPlanning: capacityPlanningRelations,
      totalLinks: incidentRelations.length + capacityPlanningRelations.length,
    };
  }

  getPriority(labels) {
    for (let l of ["priority::1", "priority::2", "priority::3", "priority::4"]) {
      if (labels.has(l)) return l;
    }
  }

  getSeverity(labels) {
    for (let l of ["severity::1", "severity::2", "severity::3", "severity::4"]) {
      if (labels.has(l)) return l;
    }
  }

  async categorizeIssue(issue) {
    let results = new Set();
    let hiddenResults = new Set();
    hiddenResults.add("ALL");

    let labels = new Set(issue.labels);
    let createdAt = parseLocalDate(issue.created_at);

    let firstEvents = await this.getLabelEvents(issue);

    let relatedLinks = await this.categorizeRelatedIssues(issue);

    let priority = this.getPriority(labels);
    let severity = this.getSeverity(labels);

    issue.priority = priority;
    issue.severity = severity;
    issue.relatedLinks = relatedLinks;

    // Priorities
    let prioritySetDate = firstEvents.get(priority);
    let prioritySLADays = PRIORITY_SLA_DAYS[priority];
    if (prioritySetDate && prioritySLADays) {
      hiddenResults.add("HAS_PRIORITY_SLA");

      let prioritySLADate = addDays(prioritySetDate, prioritySLADays);
      issue.prioritySLADate = prioritySLADate;

      if (isBefore(prioritySLADate, Date.now())) {
        results.add("PRIORITY_SLA_VIOLATED");
      }
    }

    // Severities
    let severitySetDate = firstEvents.get(severity);
    let severitySLADays = SEVERITY_SLA_DAYS[severity];
    if (severitySetDate && severitySLADays) {
      hiddenResults.add("HAS_SEVERITY_SLA");

      let severitySLADate = addDays(prioritySetDate, prioritySLADays);
      issue.severitySLADate = severitySLADate;

      if (isBefore(severitySLADate, Date.now())) {
        results.add("SEVERITY_SLA_VIOLATED");
      }
    }

    if (!issue.milestone) {
      results.add("NO_MILESTONE");

      if (differenceInDays(Date.now(), createdAt) >= 30) {
        results.add("STAGNANT");
      }
    }

    if (issue.milestone && issue.milestone.due_date) {
      let dueDate = parseLocalDate(issue.milestone.due_date);
      issue.milestoneDueDate = dueDate;

      if (isBefore(dueDate, Date.now())) {
        if (labels.has("workflow::verification")) {
          results.add("PAST_DUE_VERIFICATION");
        } else if (labels.has("workflow::in review")) {
          results.add("PAST_DUE_IN_REVIEW");
        } else if (!issue.assignee) {
          results.add("PAST_DUE_UNASSIGNED");
        } else {
          results.add("PAST_DUE");
        }
      }
    }

    if (issue.milestone && issue.milestone.start_date) {
      if (isPastDue(issue.milestone.start_date) && !isPastDue(issue.milestone.due_date)) {
        results.add("CURRENT_MILESTONE");

        if (!issue.assignee) {
          results.add("CURRENT_MILESTONE_BUT_UNASSIGNED");
        }
      }
    }

    if (issue.milestone && issue.milestone.title === "Backlog") {
      results.add("BACKLOG");

      if (differenceInDays(Date.now(), createdAt) >= 90) {
        results.add("STAGNANT");
      }
    }

    let firstInfradevLabellingDate = firstEvents.get("infradev");
    if (differenceInDays(Date.now(), firstInfradevLabellingDate) <= 14) {
      if (relatedLinks.totalLinks === 0) {
        results.add("RECENT_ISSUE_WITHOUT_LINKS");
      } else {
        results.add("RECENT_ISSUE");
      }
    }

    hiddenResults.add("IS_ASSIGNABLE");
    if (issue.assignee) {
      hiddenResults.add("IS_ASSIGNED");
    }

    if (results.size === 0) {
      results.add("OTHER");
    }
    hiddenResults.forEach((f) => results.add(f));

    return results;
  }

  async categorizeInfradevIssues() {
    let issueCategories = new Map();
    function add(category, issue) {
      if (issueCategories.has(category)) {
        issueCategories.get(category).push(issue);
      } else {
        issueCategories.set(category, [issue]);
      }
    }

    for await (const issue of urlStreamer("https://gitlab.com/api/v4/groups/gitlab-org/issues?labels=infradev&scope=all&state=opened&confidential=true")) {
      let categories = await this.categorizeIssue(issue);
      issue.categories = categories;
      categories.forEach((category) => add(category, issue));
    }

    for await (const issue of urlStreamer("https://gitlab.com/api/v4/groups/gitlab-org/issues?labels=infradev&scope=all&state=opened&confidential=false")) {
      let categories = await this.categorizeIssue(issue);
      issue.categories = categories;
      categories.forEach((category) => add(category, issue));
    }

    return issueCategories;
  }
}

module.exports = InfradevIssueCategorizer;
