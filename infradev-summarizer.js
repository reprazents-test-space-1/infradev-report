"use strict";

const differenceInWeeks = require("date-fns/difference_in_weeks");
const simpleStats = require("simple-statistics");

function ageInWeeks(date) {
  return differenceInWeeks(Date.now(), date);
}

function dueInWeeks(date) {
  return differenceInWeeks(date, Date.now());
}

function max(items, fn) {
  let arr = items.map(fn);
  if (!arr.length) return null;
  return simpleStats.max(arr);
}

function median(items, fn) {
  let arr = items.map(fn).filter((f) => !!f);
  if (!arr.length) return null;
  return simpleStats.median(arr);
}

function display(value, suffix, factor = 1, decimals = 0) {
  if (Number.isNaN(value) || value === null) {
    return "--";
  } else {
    return (value * factor).toFixed(decimals) + " " + suffix;
  }
}

function percentageItems(items, fn) {
  let matchingCount = 0;
  let totalCount = 0;
  items.forEach((item) => {
    let r = fn(item);
    if (r === null) return;
    totalCount++;
    if (r) {
      matchingCount++;
    }
  });

  return matchingCount / totalCount;
}

class InfradevSummarizer {
  constructor(title, items, grouperFilter, percentageCategoryTitle, percentageMatcherFn, slaDateFn) {
    this.title = title;
    this.items = items;
    this.grouperFilter = grouperFilter;
    this.percentageCategoryTitle = percentageCategoryTitle;
    this.percentageMatcherFn = percentageMatcherFn;
    this.slaDateFn = slaDateFn;
  }

  render() {
    let bodyLines = [`## Summary by ${this.title}`];
    bodyLines.push(`|**${this.title}**|**Count**|**Oldest**|**${this.percentageCategoryTitle}**|**Median Age**|**Median Due Time**|% Assigned|`);
    bodyLines.push("|-----------------|---------|----------|-----------------------------------|--------------|-------------------|----------|");

    let groups = this.items.reduce((memo, item) => {
      let key = this.grouperFilter(item);
      if (key !== null) {
        if (memo.has(key)) {
          memo.get(key).push(item);
        } else {
          memo.set(key, [item]);
        }
      }

      return memo;
    }, new Map());

    let keys = Array.from(groups.keys());
    keys.sort();

    keys.forEach((key) => {
      let values = groups.get(key);
      let oldest = max(values, (item) => ageInWeeks(item.created_at));
      let matchingRatio = percentageItems(values, this.percentageMatcherFn);
      let medianAge = median(values, (item) => ageInWeeks(item.created_at));
      let medianSLA = median(values, (item) => {
        let date = this.slaDateFn(item);
        if (date) return dueInWeeks(date);
      });
      let assignedRatio = percentageItems(values, (issue) => (issue.categories.has("IS_ASSIGNABLE") ? issue.categories.has("IS_ASSIGNED") : null));

      let cells = [
        key,
        values.length,
        display(oldest, " weeks"),
        display(matchingRatio, "%", 100, 1),
        display(medianAge, " weeks"),
        display(medianSLA, " weeks"),
        display(assignedRatio, "%", 100, 1),
      ];

      bodyLines.push(`|${cells.join("|")}|`);
    });

    return bodyLines;
  }
}

module.exports = InfradevSummarizer;
