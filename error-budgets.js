/* eslint-disable no-console */
"use strict";

const { PROMETHEUS_ENDPOINT } = process.env;
const { PrometheusDriver } = require("prometheus-query");
var startOfDay = require("date-fns/start_of_day");

const prom = new PrometheusDriver({
  endpoint: PROMETHEUS_ENDPOINT || "http://localhost:10902",
});

const queryBudgets = `
  clamp_max(
    # Number of successful measurements
    sum by (stage_group)(
      # Request with satisfactory apdex
      label_replace(
        sum_over_time(
          gitlab:component:stage_group:execution:apdex:success:rate_1h{environment="gprd",monitor="global",stage="main"}[28d]
        ),
        "c", "apdex", "", ""
      )
      or
      label_replace(
        # Requests without error
        (
          sum by (stage_group) (
            sum_over_time(
              gitlab:component:stage_group:execution:ops:rate_1h{environment="gprd",monitor="global",stage="main"}[28d]
            )
          )
          -
          sum by (stage_group) (
            sum_over_time(
              gitlab:component:stage_group:execution:error:rate_1h{environment="gprd",monitor="global",stage="main"}[28d]
            )
          )
        ),
        "c", "error", "", ""
      )
    )
    /
    # Number of measurements
    sum by (stage_group)(
      label_replace(
        # Apdex Measurements
        sum_over_time(
          gitlab:component:stage_group:execution:apdex:weight:score_1h{environment="gprd",monitor="global",stage="main"}[28d]
        ),
        "c", "apdex", "", ""
      )
      or
      # Requests
      label_replace(
        sum_over_time(
          gitlab:component:stage_group:execution:ops:rate_1h{environment="gprd",monitor="global",stage="main"}[28d]
        ),
        "c", "error", "", ""
      )
    ),
  1)
`;

const apdexComponentQuery = `
  clamp_max(
    # Number of successful measurements
    sum by (stage_group)(
      # Request with satisfactory apdex
      sum_over_time(
        gitlab:component:stage_group:execution:apdex:success:rate_1h{environment="gprd",monitor="global",stage="main"}[28d]
      )
    )
    /
    # Number of measurements
    sum by (stage_group)(
      # Apdex Measurements
      sum_over_time(
        gitlab:component:stage_group:execution:apdex:weight:score_1h{environment="gprd",monitor="global",stage="main"}[28d]
      )
    ),
  1)
`;

const errorComponentQuery = `
clamp_max(
  # Number of successful measurements
  1 -
  sum by (stage_group)(
    # Requests without error
    (
      sum_over_time(
        gitlab:component:stage_group:execution:error:rate_1h{environment="gprd",monitor="global",stage="main"}[28d]
      )
    )
  )
  /
  # Number of measurements
  sum by (stage_group)(
    # Requests
    sum_over_time(
      gitlab:component:stage_group:execution:ops:rate_1h{environment="gprd",monitor="global",stage="main"}[28d]
    )
  ),
1)
`;

const queryShares = `
  sum by (stage_group)(
    # Apdex Measurements
    label_replace(
      sum_over_time(
        gitlab:component:stage_group:execution:apdex:weight:score_1h{environment="gprd",monitor="global",stage="main"}[28d]
      ),
      "c", "apdex", "", ""
    )
    or
    # Requests
    label_replace(
      sum_over_time(
        gitlab:component:stage_group:execution:ops:rate_1h{environment="gprd",monitor="global",stage="main"}[28d]
      ),
      "c", "error", "", ""
    )
  )
  / ignoring(stage_group) group_left()
  sum by () (
    # Apdex Measurements
    label_replace(
      sum_over_time(
        gitlab:component:stage_group:execution:apdex:weight:score_1h{environment="gprd",monitor="global",stage="main"}[28d]
      ),
      "c", "apdex", "", ""
    )
    or
    # Requests
    label_replace(
      sum_over_time(
        gitlab:component:stage_group:execution:ops:rate_1h{environment="gprd",monitor="global",stage="main"}[28d]
      ),
      "c", "error", "", ""
    )
  )
`;

function indexByStageGroup(result) {
  return result.reduce((memo, series) => {
    let stageGroup = series.metric.labels.stage_group;
    memo[stageGroup] = series.value.value;

    return memo;
  }, {});
}

async function instantQuery(query, time) {
  let retries = 3;
  for (;;) {
    try {
      console.error("# promql query");
      return await prom.instantQuery(query, time);
    } catch (e) {
      console.error("# promql failed");
      retries--;

      if (retries <= 0) {
        throw new Error(`promql query failed: query=${query}: ${JSON.stringify(e)}`);
      } else {
        // Sleep for a second
        await new Promise((resolve) => setTimeout(resolve, 1000));
      }
    }
  }
}

async function fetchErrorBudgets() {
  const midnight = startOfDay(Date.now());

  const shareResponse = await instantQuery(queryShares, midnight);
  let shares = indexByStageGroup(shareResponse.result);

  const apdexResponse = await instantQuery(apdexComponentQuery, midnight);
  let apdexComponent = indexByStageGroup(apdexResponse.result);

  const errorResponse = await instantQuery(errorComponentQuery, midnight);
  let errorComponent = indexByStageGroup(errorResponse.result);

  const res = await instantQuery(queryBudgets, midnight);
  const seriesBudgets = res.result;
  return seriesBudgets.reduce((memo, series) => {
    let stageGroup = series.metric.labels.stage_group;

    memo[stageGroup] = {
      budget: series.value.value,
      share: shares[stageGroup],
      apdexComponent: apdexComponent[stageGroup],
      errorComponent: errorComponent[stageGroup],
    };

    return memo;
  }, {});
}

module.exports = fetchErrorBudgets;
