"use strict";

const urlStreamer = require("./url-streamer");
const setHours = require("date-fns/set_hours");
const setMinutes = require("date-fns/set_minutes");
const setSeconds = require("date-fns/set_seconds");
const setMilliseconds = require("date-fns/set_milliseconds");
const isBefore = require("date-fns/is_before");
var differenceInDays = require("date-fns/difference_in_days");
var addDays = require("date-fns/add_days");
const parse = require("date-fns/parse");

// https://about.gitlab.com/handbook/engineering/quality/issue-triage/#priority
const PRIORITY_SLA_DAYS = {
  "priority::1": 30,
  "priority::2": 90,
};

// https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity
const SEVERITY_SLA_DAYS = {
  "severity::1": 30,
  "severity::2": 60,
  "severity::3": 90,
};

function parseLocalDate(date) {
  let d = parse(date);
  d = setHours(d, 0);
  d = setMinutes(d, 0);
  d = setSeconds(d, 0);
  d = setMilliseconds(d, 0);
  return d;
}

class InfradevEpicCategorizer {
  constructor() {}

  async getLabelEvents(epic) {
    let firstEvents = new Map();

    try {
      for await (const event of urlStreamer(`https://gitlab.com/api/v4/groups/${epic.group_id}/epics/${epic.iid}/resource_label_events`)) {
        if (event.action !== "add" || !event.label) {
          continue;
        }

        let createdAt = parse(event.created_at);
        let label = event.label.name;
        if (!firstEvents.has(label)) {
          firstEvents.set(label, createdAt);
        }
      }
    } catch (e) {} // eslint-disable-line no-empty
    return firstEvents;
  }

  getPriority(labels) {
    for (let l of ["priority::1", "priority::2", "priority::3", "priority::4"]) {
      if (labels.has(l)) return l;
    }
  }

  getSeverity(labels) {
    for (let l of ["severity::1", "severity::2", "severity::3", "severity::4"]) {
      if (labels.has(l)) return l;
    }
  }

  isSLAViolation(startDate, slaDays) {
    if (!startDate || !slaDays) return false;
    let slaDate = addDays(startDate, slaDays);
    return isBefore(slaDate, Date.now());
  }

  async categorizeEpic(epic) {
    let results = new Set();
    let hiddenResults = new Set();
    hiddenResults.add("ALL");

    let labels = new Set(epic.labels);
    let dueDate = parseLocalDate(epic.due_date);
    let createdAt = parse(epic.created_at);

    let firstEvents = await this.getLabelEvents(epic);

    // let relatedLinks = await this.categorizeRelatedIssues(epic);

    let priority = this.getPriority(labels);
    let severity = this.getSeverity(labels);

    epic.priority = priority;
    epic.severity = severity;
    epic.relatedLinks = { incidents: [], capacityPlanning: [], totalLinks: 0 };

    let prioritySetDate = firstEvents.get(priority) || createdAt;
    let severitySetDate = firstEvents.get(severity) || createdAt;

    let prioritySLADays = PRIORITY_SLA_DAYS[priority];
    let severitySLADays = SEVERITY_SLA_DAYS[severity];

    let priorityViolation = this.isSLAViolation(prioritySetDate, prioritySLADays);
    if (priorityViolation) {
      results.add("PRIORITY_SLA_VIOLATED");
    }

    let severityViolation = this.isSLAViolation(severitySetDate, severitySLADays);
    if (severityViolation) {
      results.add("SEVERITY_SLA_VIOLATED");
    }

    if (isBefore(dueDate, Date.now())) {
      results.add("EPIC_PAST_DUE");
    }

    let firstInfradevLabellingDate = firstEvents.get("infradev");
    if (differenceInDays(Date.now(), firstInfradevLabellingDate) <= 14) {
      results.add("RECENT_EPIC");
    }

    if (results.size === 0) {
      results.add("OTHER_EPIC");
    }

    hiddenResults.forEach(f => results.add(f));

    return results;
  }

  async categorizeInfradevEpics() {
    let epicCategories = new Map();
    function add(category, issue) {
      if (epicCategories.has(category)) {
        epicCategories.get(category).push(issue);
      } else {
        epicCategories.set(category, [issue]);
      }
    }

    for await (const epic of urlStreamer("https://gitlab.com/api/v4/groups/gitlab-org/epics?labels=infradev&scope=all&state=opened&confidential=true")) {
      let categories = await this.categorizeEpic(epic);
      epic.categories = categories;
      categories.forEach((category) => add(category, epic));
    }

    for await (const epic of urlStreamer("https://gitlab.com/api/v4/groups/gitlab-org/epics?labels=infradev&scope=all&state=opened&confidential=false")) {
      let categories = await this.categorizeEpic(epic);
      epic.categories = categories;
      categories.forEach((category) => add(category, epic));
    }

    return epicCategories;
  }
}

module.exports = InfradevEpicCategorizer;
