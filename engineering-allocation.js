const fetchStageGroups = require("./stage-groups");
const fetchErrorBudgets = require("./error-budgets");
const _ = require("lodash");
const httpClient = require("./http-client");
const startOfMonth = require("date-fns/start_of_month");
const getTime = require("date-fns/get_time");

const PRODUCTION_PROJECT = "gitlab-com/gl-infra/production";
const PRODUCTION_PROJECT_FULL_URL = `https://gitlab.com/${PRODUCTION_PROJECT}`;
const issueUrlRegex = new RegExp(`^${_.escapeRegExp(PRODUCTION_PROJECT_FULL_URL + "/-/issues/")}(.*)`);

function labelsMatch(a, b) {
  return a.toLowerCase().replace(/ /g, "_") == b.toLowerCase().replace(/ /g, "_");
}

function display(value, suffix = "", factor = 1, decimals = 0) {
  if (Number.isNaN(value) || value === null || value === undefined) {
    return "--";
  } else {
    return (value * factor).toFixed(decimals) + suffix;
  }
}

function getSeverity(issue) {
  for (let l of issue.labels) {
    if (l.startsWith("severity::")) {
      let [, severityText] = l.split("::");
      return parseInt(severityText, 10);
    }
  }
}

function getSevEmoji(sev) {
  switch (sev) {
    case 1:
      return ":one:";
    case 2:
      return ":two:";
    case 3:
      return ":three:";
    case 4:
      return ":four:";
    default:
      return ":hash:";
  }
}

class EngineeringAllocator {
  constructor() {}

  async fetch(allInfradevItems) {
    this.stageGroups = await fetchStageGroups();
    this.errorBudgets = await fetchErrorBudgets();
    this.incidents = await this.fetchIncidents(allInfradevItems);
  }

  // Walks through all infradev issues,
  // and fetches the related incidents
  // for further analysis
  async fetchIncidents(allInfradevItems) {
    let allIncidentIssues = allInfradevItems
      .flatMap(function (issue) {
        return issue?.relatedLinks?.incidents;
      })
      .flatMap((url) => {
        let m = issueUrlRegex.exec(url);
        if (m) {
          return [m[1]];
        } else {
          return [];
        }
      });

    let incidentMap = new Map();
    this.incidentMap = incidentMap;

    let uniqueIncidents = _.uniq(allIncidentIssues);
    for (let issueId of uniqueIncidents) {
      let [result] = await httpClient(`https://gitlab.com/api/v4/projects/${encodeURIComponent(PRODUCTION_PROJECT)}/issues/${issueId}`);
      incidentMap.set(result.web_url, result);
    }
  }

  async report(allInfradevItems) {
    await this.fetch(allInfradevItems);

    let report = this.stageGroups.map((stageGroup) => {
      let label = `group::${stageGroup.id.replace(/_/g, " ")}`;
      let budget = this.errorBudgets[stageGroup.id];
      return {
        id: stageGroup.id,
        label: label,
        labelMarkdown: `~"group::${stageGroup.id.replace(/_/g, " ")}"`,
        errorBudget: budget?.budget,
        trafficShare: budget?.share,
        trafficShareHasSignificance: budget?.share >= 0.0001,
        apdexComponent: budget?.apdexComponent,
        errorComponent: budget?.errorComponent,
        section: stageGroup.section,
        stage: stageGroup.stage,
        issueCount: this.countInfradevIssues(allInfradevItems, stageGroup.id),
        incidents: this.collectIncidents(allInfradevItems, stageGroup.id),
      };
    });

    let filtered = report.filter((item) => {
      return item.issueCount || item.incidentCount || item.errorBudget;
    });

    filtered = _.sortBy(filtered, [(o) => -o.incidents.length, (o) => -o.issueCount, (o) => !o.trafficShareHasSignificance, (o) => o.errorBudget]);

    function errorBudgetColumn(item) {
      if (!item.trafficShareHasSignificance) {
        // Don't display error budget values for stage groups with less than 0.01% of traffic
        return "";
      }

      let value = display(item.errorBudget, "%", 100, 2);
      let link = `[${value}](https://dashboards.gitlab.net/d/stage-groups-${item.id})`;

      if (item.errorBudget && item.errorBudget < 0.9995) {
        return `**${link}** ❌`;
      }

      if (item.errorBudget) {
        return `${link} 🟢`;
      }

      return link;
    }

    function apdexComponentColumn(item) {
      if (!item.trafficShareHasSignificance) {
        // Don't display error budget values for stage groups with less than 0.01% of traffic
        return "";
      }

      return display(item.apdexComponent, "%", 100, 2);
    }

    function errorComponentColumn(item) {
      if (!item.trafficShareHasSignificance) {
        // Don't display error budget values for stage groups with less than 0.01% of traffic
        return "";
      }

      return display(item.errorComponent, "%", 100, 2);
    }

    function infradevColumn(item) {
      let value = display(item.issueCount);
      if (item.issueCount && item.issueCount > 0) {
        value = `**${value}**`;
      }

      let handbookReportPermalink = item.id.replace(/ _/g, "-");
      let handbookReport = `https://about.gitlab.com/handbook/engineering/metrics/${item.section}/${item.stage}/#${handbookReportPermalink}-group-infrastructure-dashboard`;

      return `[${value}](http://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=infradev&label_name[]=${encodeURIComponent(
        item.label
      )}) <small>[📊](${handbookReport})</small>`;
    }

    function incidentColumn(item) {
      let incidentCount = item.incidents.length;
      let value = display(incidentCount);
      if (incidentCount && incidentCount > 0) {
        value = `**${value}**`;
      }
      return value;
    }

    function incidentLinksColumn(item) {
      let month = null;

      let incidentMarkdowns = item.incidents.map((i) => {
        let m = getTime(startOfMonth(i.created_at));
        let monthSep = "";

        if (month && month !== m) {
          // Add a separator when month of incident changes
          monthSep = " \\| ";
        }
        month = m;

        let sev = getSeverity(i);
        return `${monthSep}[${getSevEmoji(sev)}](${i.web_url})`;
      });

      return incidentMarkdowns.join(" ");
    }

    function trafficShareColumn(item) {
      let value = display(item.trafficShare, "%", 100, 3);
      if (item.trafficShareHasSignificance) {
        value = `**${value}**`;
      }

      return value;
    }

    return [
      "| **Section** | **Group** | **Traffic Share**|**Error Budget** | **Apdex Comp.** | **Error Comp.** | **Infradev Issues** | **Related Incidents via Infradev Issues** | **Incidents** |",
      "|-------------|-----------|------------------|-----------------|-----------------|-----------------|---------------------|-----------------------|-----------|",
    ].concat(
      filtered.flatMap((item) => {
        let cells = [
          `~"section::${item.section}"`,
          item.labelMarkdown,
          trafficShareColumn(item),
          errorBudgetColumn(item),
          apdexComponentColumn(item),
          errorComponentColumn(item),
          infradevColumn(item),
          incidentColumn(item),
          incidentLinksColumn(item),
        ];

        return `|${cells.join("|")}|`;
      })
    );
  }

  collectIncidents(issues, stageGroup) {
    let stageGroupLabel = `group::${stageGroup}`;
    let allIssues = issues.flatMap(function (issue) {
      let i = issue.labels.findIndex((item) => labelsMatch(item, stageGroupLabel));
      if (i >= 0) {
        return issue?.relatedLinks?.incidents;
      } else {
        return [];
      }
    });

    let uniqueIssuesUrls = _.uniq(allIssues);

    let incidentIssues = uniqueIssuesUrls.flatMap((webUrl) => {
      let issue = this.incidentMap.get(webUrl);
      if (issue && issue.labels.includes("incident")) {
        return [issue];
      }

      return [];
    });

    return _.sortBy(incidentIssues, (i) => i.created_at);
  }

  countInfradevIssues(issues, stageGroup) {
    let stageGroupLabel = `group::${stageGroup}`;
    return issues.reduce(function (count, issue) {
      let i = issue.labels.findIndex((item) => labelsMatch(item, stageGroupLabel));
      if (i >= 0) {
        return count + 1;
      }
      return count;
    }, 0);
  }
}

module.exports = EngineeringAllocator;
