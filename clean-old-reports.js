"use strict";

const httpClient = require("./http-client");
const config = require("./config.json");

async function cleanOldReports(currentIid) {
  let [openReports] = await httpClient(
    `https://gitlab.com/api/v4/projects/${encodeURIComponent(config.project)}/issues?state=opened&scope=all&labels=Infradev+Status+Report&confidential=true`
  );

  let overdueReports = openReports.filter((issue) => issue.iid !== currentIid && issue.title.startsWith(config.prefix));

  for (const report of overdueReports) {
    await httpClient(`https://gitlab.com/api/v4/projects/${encodeURIComponent(config.project)}/issues/${report.iid}?state_event=close`, "PUT");
  }
}

module.exports = cleanOldReports;
