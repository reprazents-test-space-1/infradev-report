"use strict";

const fetch = require("./fetch");
const yaml = require("js-yaml");

async function fetchStageGroups() {
  let res = await fetch("https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/stages.yml");

  let text = await res.text();
  let stages = yaml.load(text).stages;

  return Object.keys(stages).flatMap((stageId) => {
    let v = stages[stageId];
    v.id = stageId;
    let section = v.section;

    return Object.keys(v.groups).map((key) => {
      let w = v.groups[key];
      w.id = key;
      w.stage = stageId;
      w.section = section;
      return w;
    });
  });
}

module.exports = fetchStageGroups;
