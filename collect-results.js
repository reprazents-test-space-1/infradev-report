"use strict";

var urlStreamer = require("./url-streamer");

async function collectResults(url, stopper) {
  let collect = [];

  for await (const result of urlStreamer(url, stopper)) {
    collect.push(result);
  }

  return collect;
}

module.exports = collectResults;
